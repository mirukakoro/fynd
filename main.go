package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"sync"
)

type Window struct {
	fyne.Window
	Search *widget.Entry
	Close  *widget.Button
	List   *widget.List

	noCopy sync.Mutex
}

func main() {
	a := app.NewWithID("io.gitlab.colourdelete.fynd")
	w := &Window{
		Window: a.NewWindow("Fynd"),
		Search: widget.NewEntry(),
		Close: widget.NewButtonWithIcon(
			"Close",
			theme.CancelIcon(),
			func() {
				a.Quit()
			},
		),
		List: widget.NewList(
			func() int {
				return 0
			},
			func() fyne.CanvasObject {
				return nil
			},
			func(id widget.ListItemID, object fyne.CanvasObject) {
			},
		),
	}

	w.SetContent(container.NewBorder(
		w.Search,
		widget.NewLabel("Fynd what you want to see."),
		nil,
		w.Close,
		w.List,
	))
	//w.SetFullScreen(true)
	w.Show()
	a.Run()
}
